

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import shapes3D.Cuboid;
import shapes3D.Line3D;
import shapes3D.Point3D;
import shapes3D.Polygon3D;
import shapes3D.Shape3D;
import shapes3D.Sphere;

public class GraphicsView3D extends JPanel{
	
	private static final long serialVersionUID = 1L;

	public class GraphicsObject3D{
		private Shape3D shape3D;
		private Color color;
		private double width;
		private boolean fill;
		private String text = null;
		public GraphicsObject3D(Shape3D shape3D, Color color, double width, boolean fill) {
			super();
			this.shape3D = shape3D;
			this.color = color;
			this.width = width;
			this.fill  = fill;
		}

		public GraphicsObject3D(String text, Shape3D p, Color color){
			this.text = text;
			this.shape3D = p;
			this.color = color;
		}
	}
	
	int nObjects = 0;
	
	//"zoom"
	double c = 300;
	
	//center of screen
	double xH;
	double yH;
	
	//viewpoint
	double[] X0 = new double[]{0, -1000, 1000};
	Point3D projC = new Point3D.Double(X0[0], X0[1], X0[2]);
	
	//distance of view point to origin + orientation of view (to watch the origin)
	double r = Math.sqrt(X0[0]*X0[0] + X0[1]*X0[1] + X0[2]*X0[2]);
	double alpha = Math.asin(X0[2]/r);
	double gamma = Math.atan2(X0[0], X0[1]);
	
	//normal of the picture plane
	double[] normal = null;
	
	//List and Set of all Objects to be drawn (Set sorted by distance of viewpoint to center of Object)
	public List<GraphicsObject3D> objects = new ArrayList<GraphicsObject3D>();
	private TreeSet<GraphicsObject3D> objectsSet = new TreeSet<GraphicsObject3D>(new Comparator<GraphicsObject3D>() {
		@Override
		public int compare(GraphicsObject3D o1, GraphicsObject3D o2) {
			double d1 = projC.distance(o1.shape3D.center());
			double d2 = projC.distance(o2.shape3D.center());
			return (d1 >= d2) ? -1 : 1;
		}
	});
	
	//bounding box (picture) bounding cuboid (objects)
	private Rectangle2D boundingBox = new Rectangle2D.Double(0, 0, 1, 1);
	private Cuboid boundingCuboid = null;//new Cuboid.Double();
//	private Point2D center;
	
	private int[][] CuboidSideIdx = new int[][] {
		{0, 1, 5, 4}, 
		{0, 4, 7, 3}, 
		{2, 3, 7, 6}, 
		{1, 2, 6, 5},
		{0, 1, 2, 3},
		{4, 5, 6, 7}
	};
	
	private Point2D currentMousePos = null;
	private int mouseButton = 0;
	private boolean ctrlDown = false;
			
	public GraphicsView3D() {
		addMouseWheelListener(new MouseWheelListener() {
			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				if(e.getWheelRotation() < 0){
					zoomIn();
				}
				if(e.getWheelRotation() > 0){
					zoomOut();
				}
			}
		});
		
		addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				super.mouseDragged(e);
				if(ctrlDown) {
					riseSink(e.getPoint());
				}else {
					if(mouseButton == 1) {
						translate(e.getPoint());
					}else if(mouseButton == 3) {
						rotate(e.getPoint());
					}
				}	
				currentMousePos = e.getPoint();
			}	
		});
		 
		 addMouseListener(new MouseAdapter() {
			 @Override
			public void mousePressed(MouseEvent e) {
				super.mousePressed(e);	
				ctrlDown = e.isControlDown();
				currentMousePos = e.getPoint();
				mouseButton = e.getButton();
			}	 
			@Override
			public void mouseDragged(MouseEvent e) {
				super.mouseDragged(e);
				currentMousePos = null;
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				super.mouseReleased(e);
				currentMousePos = null;
			}
		});
	}
	
	public List<GraphicsObject3D> getObjects() {
		return objects;
	}
	
	public int getnObjects() {
		return nObjects;
	}
	
	@Override
	public void paint(Graphics g) {
		
		//compute normal
		normal = new double[]{Math.sin(-gamma)*Math.sin(-alpha),
				Math.cos(-gamma)*-Math.sin(-alpha), 
				Math.cos(-alpha)};
				
		//update view point
		projC = new Point3D.Double(X0[0], X0[1], X0[2]);

		xH = getWidth()/2;
		yH = getHeight()/2;

		super.paint(g);

		g.setColor(new Color(255,255,255));
		g.fillRect(0, 0, this.getWidth(),this.getHeight());
		Graphics2D g2 = (Graphics2D) g;
						
		//update set  of Objects
		objectsSet.clear();
		objectsSet.addAll(objects);

		for(GraphicsObject3D go : objectsSet){
			g2.setColor(go.color);
			g2.setStroke(new BasicStroke((float)(go.width)));
			if(go.shape3D instanceof Sphere) {
				Point2D p;
				if(go.text != null) {
					p = projectPoint(go.shape3D.center());
					if(p != null) {
						g2.drawString(go.text, (float)p.getX(), (float)p.getY());
					}
				}else {
					Point3D c = go.shape3D.center();
					p = projectPoint(c);
					if(p != null) {
						//--- not sure ---
						double r = ((Sphere)go.shape3D).getRadius();
						double d = c.distance(projC);
						double r2 =  r / Math.sqrt(d * d - r * r) * getHeight() / 2;
						//----------------
						if(go.fill) {
							g2.fill(new Ellipse2D.Double(p.getX()-r2/2, p.getY()-r2/2, r2, r2));
						}else {
							g2.draw(new Ellipse2D.Double(p.getX()-r2/2, p.getY()-r2/2, r2, r2));
						}
					}
				}
			}
			if(go.shape3D instanceof Cuboid) {
				List<Path2D> drawnSides = projectCuboid((Cuboid)go.shape3D);
				for(Path2D poly : drawnSides) {
					g2.fill(poly);
					g2.setColor(Color.black);
					g2.draw(poly);
					g2.setColor(go.color);
				}
			}
			if(go.shape3D instanceof Polygon3D) {
				Path2D poly  = projectPolygon((Polygon3D)go.shape3D);
				if(go.fill) {
					if(poly != null)g2.fill(poly);
				}else {
					if(poly != null)g2.draw(poly);
				}
			}
			if(go.shape3D instanceof Line3D) {
				Line2D l = projectLine((Line3D)go.shape3D);
				if(l != null)g2.draw(l);
			}
		}

//		center = new Point2D.Double(boundingBox.getCenterX(), boundingBox.getCenterY());
	}	
	
	/**
	 * projects a 3D point in the 2D plane
	 * @param p 3D point
	 * @return 2D point, null if Point lies behind image plane
	 */
	public Point2D projectPoint(Point3D p) {
		
		//check if Point lies in front of plane (if it is visible)
		double[] vecX0P = new double[] {p.getX()-X0[0], 
										p.getY()-X0[1], 
										p.getZ()-X0[2]};
		double testV = normal[0]*vecX0P[0] 
					 + normal[1]*vecX0P[1] 
				     + normal[2]*vecX0P[2];	
		if(testV > 0) {
			return null;
		} 
		
		//projection
		double s = ( Math.sin(alpha)*Math.sin(gamma)*(p.getX()-X0[0]) 
				+ 	 Math.sin(alpha)*Math.cos(gamma)*(p.getY()-X0[1]) 
				+	 Math.cos(alpha)*                (p.getZ()-X0[2]) );
		double x = c * ( Math.cos(gamma)				*(p.getX()-X0[0]) 
					+ 	-Math.sin(gamma)				*(p.getY()-X0[1]) )/s + xH;
		double y = c * ( Math.cos(alpha)*Math.sin(gamma)*(p.getX()-X0[0]) 
					+ 	 Math.cos(alpha)*Math.cos(gamma)*(p.getY()-X0[1]) 
					+	-Math.sin(alpha)*                (p.getZ()-X0[2]) )/s + yH;
		
		Point2D p0 = new Point2D.Double(x, getHeight()-y); 
		boundingBox.add(p0);
		return p0;
		
	}
	
	/**
	 * projects a 3D line in the 2D plane
	 * @param l 3D line
	 * @return 2D line, null if one point of 3D line lies behind image plane
	 */
	public Line2D projectLine(Line3D l) {
		Point2D p1 = projectPoint(l.getP1());
		Point2D p2 = projectPoint(l.getP2());
		if(p1 == null || p2 == null)return null;
		return new Line2D.Double(p1, p2);
	}
	
	/**
	 * projects a 3D polygon in the 2D plane
	 * @param poly 3D polygon
	 * @return 2D polygon, null if one point of 3D polygon lies behind image plane
	 */
	public Path2D projectPolygon(Polygon3D poly) {
		Path2D poly2 = new Path2D.Double();
		int i = 0;
		for(Point3D p:poly.getPoints()) {
			Point2D p2 = projectPoint(p);
			if(p2 == null)return null;
			if(i == 0) {
				poly2.moveTo(p2.getX(), p2.getY());
			}else{
				poly2.lineTo(p2.getX(), p2.getY());
			}
			i++;
		}
		poly2.closePath();
		return poly2;
	}
	
	public List<Path2D> projectCuboid(Cuboid c) {
		List<Path2D> sides = new ArrayList<>();
		
		boolean[] seenSides = new boolean[6];
		Arrays.fill(seenSides, true);
		
		if(X0[2] > c.getZ()) {
			seenSides[4] = false;
		}
		if(X0[2] < c.getZ() + c.getHeight()) {
			seenSides[5] = false;
		}
		if(X0[1] > c.getY()) {
			seenSides[0] = false;
		}
		if(X0[1] < c.getY() + c.getWidth()) {
			seenSides[2] = false;
		}
		if(X0[0] > c.getX()) {
			seenSides[1] = false;
		}
		if(X0[0] < c.getX() + c.getLength()) {
			seenSides[3] = false;
		}
		
		Point2D[] indexedPoints = new Point2D[8];
		if(seenSides[0] || seenSides[1] || seenSides[4]) 
			indexedPoints[0] = projectPoint(new Point3D.Double(c.getX(), c.getY(), c.getZ()));
		if(seenSides[0] || seenSides[3] || seenSides[4]) 
			indexedPoints[1] = projectPoint(new Point3D.Double(c.getX() + c.getLength(), c.getY(), c.getZ()));
		if(seenSides[2] || seenSides[3] || seenSides[4])
			indexedPoints[2] = projectPoint(new Point3D.Double(c.getX() + c.getLength(), c.getY() + c.getWidth(), c.getZ()));
		if(seenSides[1] || seenSides[2] || seenSides[4])
			indexedPoints[3] = projectPoint(new Point3D.Double(c.getX(), c.getY() + c.getWidth(), c.getZ()));
		if(seenSides[0] || seenSides[1] || seenSides[5])
			indexedPoints[4] = projectPoint(new Point3D.Double(c.getX(), c.getY(), c.getZ() + c.getHeight()));
		if(seenSides[0] || seenSides[3] || seenSides[5])
			indexedPoints[5] = projectPoint(new Point3D.Double(c.getX() + c.getLength(), c.getY(), c.getZ() + c.getHeight()));
		if(seenSides[2] || seenSides[3] || seenSides[5])
			indexedPoints[6] = projectPoint(new Point3D.Double(c.getX() + c.getLength(), c.getY() + c.getWidth(), c.getZ() + c.getHeight()));
		if(seenSides[1] || seenSides[2] || seenSides[5])
			indexedPoints[7] = projectPoint(new Point3D.Double(c.getX(), c.getY() + c.getWidth(), c.getZ() + c.getHeight()));
		
		for(int i = 0; i < 6; i++) {
			if(seenSides[i]) {
				Path2D poly2 = new Path2D.Double();
				for(int j = 0; j < 4; j++) {
					int idx = CuboidSideIdx[i][j];
					Point2D p2 = indexedPoints[idx];
					if(p2 == null) {
						poly2 = null;
						break;
					}
					if(j == 0) {
						poly2.moveTo(p2.getX(), p2.getY());
					}else{
						poly2.lineTo(p2.getX(), p2.getY());
					}
				}
				if(poly2 != null) {
					poly2.closePath();
					sides.add(poly2);
				}
			}
		}
		
		return sides;
	}
		
//	public void moveToCenter(){
//		this.c = 300;
//		
//		SwingUtilities.invokeLater(new Runnable() {
//			
//		@Override
//		public void run() {
//			center.setLocation(boundingBox.getCenterX(),boundingBox.getCenterY());
//			refresh();			
//		};
//		});
//	}

	public GraphicsObject3D addText(String text, Point3D location){
		nObjects++;
		if(boundingCuboid  != null) {
			boundingCuboid.extendToInclude(location);
		}else {
			boundingCuboid = new Cuboid.Double(location.getX(), location.getY(), location.getZ(), 1, 1, 1);
		}
		
		GraphicsObject3D tObj = new GraphicsObject3D(text, new Sphere(location, 1), new Color(1, 0, 0));
		
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				objects.add(tObj);
				refresh();
			}
		});
				
		return tObj;
	}
	
	
	public GraphicsObject3D addShape(Shape3D shape){
		return addShape(shape, new Color(0, 0, 0),1);
	}
	
	public GraphicsObject3D addShape(Shape3D shape, Color color){
		return addShape(shape, color, 1);
	}
	
	public GraphicsObject3D addLine(Line3D l) {
		return addLine(l.getP1(), l.getP2(), new Color(0, 0, 0), 1);
	}
	
	public GraphicsObject3D addLine(Point3D p1, Point3D p2){
		return addLine(p1,p2,new Color(0,0,0),1);
	}
	
	public GraphicsObject3D addLine(Point3D p1, Point3D p2, Color color, double width){
		return addShape(new Line3D.Double(p1, p2),color,width);
	}
	
	public GraphicsObject3D addPoint(Point3D p){
		return addPoint(p, new Color(0,0,0), 1);
	}
	
	public GraphicsObject3D addPoint(Point3D p, Color color, double width) {
		return addShape(new Sphere(p, 1), color, width);
	}
	
	public GraphicsObject3D addPoint(Point3D p, Color color, double radius, double width) {
		return addShape(new Sphere(p, radius), color, width);
	}

	public GraphicsObject3D addShape(Shape3D shape, Color color, double width){
		return addShape(shape, color, width, true);
	}
	
	public GraphicsObject3D addShape(Shape3D shape, Color color, double width, boolean fill){
		nObjects++;
		if(boundingCuboid  != null) {
			boundingCuboid.extendToInclude(shape.getBounds3D());
		}else {
			boundingCuboid = new Cuboid.Double(shape.getBounds3D().getX(), 
					shape.getBounds3D().getY(), 
					shape.getBounds3D().getZ(), 
					shape.getBounds3D().getLength(), 
					shape.getBounds3D().getWidth(), 
					shape.getBounds3D().getHeight());
		}
		
		GraphicsObject3D shapeObj = new GraphicsObject3D(shape, color, width,fill);
		
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				objects.add(shapeObj);
				refresh();
			}
		});

		return shapeObj;
		
	}
	
	public void remove(Shape3D shape) {
		for(GraphicsObject3D gobject : objects) {
			if(gobject.shape3D.equals(shape)) {
				objects.remove(gobject);
				break;
			}
		}
		nObjects--;
		refresh();
	}
	
	public void removeAll(Collection<Shape3D> shapes) {
		for(Shape3D shape : shapes) {
			remove(shape);
		}
	}
	
	public void remove(Shape3D[] shapes) {
		for(Shape3D shape : shapes) {
			remove(shape);
		}
	}
	
	public void removeAll(ArrayList<GraphicsObject3D> obj) {
		for(GraphicsObject3D object : obj) {
			objects.remove(object);
		}
		refresh();
	}
	
	public void remove(int i) {
		objects.remove(i);
		nObjects--;
		refresh();
	}
		
	public void remove(int[] inds) {
		int removed = 0;
		for(int i = 0; i < inds.length; i++) {
			remove(inds[i]-removed);
			removed++;
		}
	}
		
	public void refresh(){
		repaint();
	}
	
	public void setViewPoint(double[] x0new) {
		if(x0new.length != 3) {
			System.out.println("X0 has exactly 3 components");
		}else {
			X0 = x0new;
			r = Math.sqrt(X0[0]*X0[0] + X0[1]*X0[1] + X0[2]*X0[2]);
			double dist = Math.sqrt(X0[0]*X0[0] + X0[1]*X0[1]);
			alpha = Math.asin(dist/r);
			gamma = Math.atan2(X0[0], X0[1]);// + Math.atan2(X0[0]-boundingCuboid.getCenterX(), X0[1]-boundingCuboid.getCenterY());
			refresh();
		}
	}
	
	public void setViewPoint(double[] x0new, double[] angles) {
		if(x0new.length != 3) {
			System.out.println("X0 has exactly 3 components");
		}else {
			X0 = x0new;
			alpha = angles[0];
			gamma = angles[1];
			refresh();
		}
	}
	
//	public void moveTo(Point2D p){
////		System.out.println(currentMousePos);
//		if(currentMousePos != null){
//			center.setLocation(center.getX()-(p.getX()-currentMousePos.getX()),center.getY()-(p.getY()-currentMousePos.getY()));
//		}
//		refresh();
//	}
	
	/**
	 * sets a default viewpoint so that: 
	 * x = center_x of bounding cuboid of objects, 
	 * y = cuboid_y, 
	 * z = 1/2 height of cuboid on top of cuboid
	 */
	public void setDefaultViewPoint() {
		setViewPoint(new double[] {boundingCuboid.getCenterX(), 
				boundingCuboid.getY(), 
				boundingCuboid.getZ() + 1.5 * boundingCuboid.getHeight()});
	}
	
	public void addEuclCoordSystem(double length, double width) {
		Point3D c = new Point3D.Double(0, 0, 0);
		addLine(c, new Point3D.Double(length, 0, 0), Color.red, width);
		addLine(c, new Point3D.Double(0, length, 0), Color.green, width);
		addLine(c, new Point3D.Double(0, 0, length), Color.blue, width);
	}
	
	/**
	 * translates the view point (only x and y)
	 * @param p
	 */
	public void translate(Point2D p) {
		double dx = Math.sin(gamma);
		double dy = Math.cos(gamma);
		
		double scale = (boundingCuboid.center().distance(new Point3D.Double(0, 0, 0))
							/projC.distance(boundingCuboid.center())) * 10;
//		if(scale > 100) scale = 1;

//		System.out.println(scale);
		
		X0[0] -= (dx * (p.getY()-currentMousePos.getY())) / scale;
		X0[1] -= (dy * (p.getY()-currentMousePos.getY())) / scale;
		
		X0[0] += (dy * (p.getX()-currentMousePos.getX())) / scale;
		X0[1] -= (dx * (p.getX()-currentMousePos.getX())) / scale;
		refresh();
	}
		
	/**
	 * translates the view point (only z), 
	 * @param p
	 */
	public void riseSink(Point2D p) {
		double scale = (boundingCuboid.center().distance(new Point3D.Double(0, 0, 0))
				/projC.distance(boundingCuboid.center())) * 10;
//		if(scale > 100) scale = 0.1;
//		System.out.println(scale);
		X0[2] += (p.getY()-currentMousePos.getY()) / scale;
		refresh();
	}
	
	/**
	 * rotate the image plane about the view point
	 * @param p
	 */
	public void rotate(Point2D p) {
		gamma += (p.getX()-currentMousePos.getX())/800;
		
		alpha -= (p.getY()-currentMousePos.getY())/800;
		
		refresh();
	}
	
	public void zoomIn(){
		c = c + 20;
		refresh();
	}
	
	public void zoomOut(){
		if(c > 0) c -= 20;
		refresh();
	}
	
	

}
