import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import shapes3D.Cuboid;
import shapes3D.Point3D;
import shapes3D.Shape3D;
import shapes3D.Sphere;

public class Tests {

	public static void main(String[] args) throws InterruptedException {
		List<Shape3D> shapes = new ArrayList<>();
		List<Color> colors = new ArrayList<>();
		List<double[]> viewPos = new ArrayList<>();
		
		double spacing = 100;
		for(int i = 0; i < 10; i++) {
			double x = i * spacing;
			double y = 0;
			double z = 0;
			double l = 50;
			double w = 50;
			double h = i * 10 + 10;
			
			shapes.add(new Cuboid.Double(x, y, z, l, w, h));
			colors.add(new Color(145, 187, 255));
		}
		for(int i = 0; i < 10; i++) {
			double x = i * spacing;
			double y = -150;
			double z = 0;
			double l = 50;
			double w = 50;
			double h = 110 - (i * 10 + 10);
			
			shapes.add(new Cuboid.Double(x, y, z, l, w, h));
			colors.add(new Color(145, 187, 255));
		}
		
//		shapes.add(new Sphere(new Point3D.Double(), 400));
//		colors.add(new Color(0, 255, 0));
		
		int seconds = 60;
		for(int i = 0; i < seconds; i++) {
			double x = 1100 / (double) seconds * (double) i;
			double y = -50;
			double z = 5 + 200 / (double) seconds * (double) i;
			
			double s =  Math.PI / 4 + (Math.PI / 2) / (double) seconds * (double) i;
			double c = 1.2 + 1.3 / (seconds) * (double) i;
			
			double a = Math.sin(s);
			double g = Math.PI * c;// + Math.cos(c);
			double[] pos = new double[] {x, y, z, a, g};
			viewPos.add(pos);
		}
				
		ShapeViewer3D pv = new ShapeViewer3D(shapes, colors);
//		pv.animate(viewPos);
//		pv.rotateAboutOrigin(10, 10, 10000);
	}

}
