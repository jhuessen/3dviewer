

import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JFrame;

import shapes3D.Point3D;
import shapes3D.Shape3D;

public class ShapeViewer3D extends JFrame{

	private static final long serialVersionUID = 1L;
	public GraphicsView3D view = new GraphicsView3D();
	
	public ShapeViewer3D(List<Shape3D> shapes, List<Color> shapeColors) {
		view = new GraphicsView3D();
		setMinimumSize(new Dimension(800,800));
		view.setSize(new Dimension(800,800));
		setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
				
		for(int i = 0; i < shapes.size(); i++) {
			view.addShape(shapes.get(i), shapeColors.get(i), 1);
		}
		
		
//		view.setViewPoint(new double[] {0, -20, 5}, new double[] {Math.PI / 2, Math.PI * 1.2});
//		view.addPoint(new Point3D.Double());
		this.add(view);
//		view.setDefaultViewPoint();
		view.refresh();		
		
	}
	
	public void addText(String text, Point3D location) {
		view.addText(text, location);
	}
	
	public void rotateAboutOrigin(double radius, double height, int frames) throws InterruptedException {
		double[] X0 = new double[] {0, 0, height};
		
		double angle = Math.PI * 2;		
		for(int i = 1; i <= frames; i++) {
			X0[0] = radius * Math.cos(angle / (double)frames * (double) i);
			X0[1] = radius * Math.sin(angle / (double)frames * (double) i);
			Thread.sleep(1000/30);
			view.setViewPoint(X0);
		}
		
	}
	
	public void animate(List<double[]> positions) throws InterruptedException {
		
		for(int i = 0; i < positions.size() - 1; i++) {
//			List<double[]> interpolatedPos = new ArrayList<>();
			double[] origin = positions.get(i);
			double[] target = positions.get(i + 1);
			
			double[] posDiff = new double[5];
			for(int j = 0; j < 5; j++) {
				posDiff[j] = target[j] - origin[j];
			}
			
			for(int j = 1; j <= 30; j++) {
				double[] pos = new double[5];
				for(int k = 0; k < 5; k++) {
					pos[k] = origin[k] + (double) j * (posDiff[k] / 30);
				}
				
				view.setViewPoint(Arrays.copyOf(pos, 3), Arrays.copyOfRange(pos, 3, 5));
				Thread.sleep(1000/30);
			}
			
			
		}
		
	}
	
	public ShapeViewer3D(Point3D[] points) {
		setMinimumSize(new Dimension(800,800));
		view.setSize(new Dimension(800,800));
		setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		view = new GraphicsView3D();
		this.add(view);
				
	}
	
}
