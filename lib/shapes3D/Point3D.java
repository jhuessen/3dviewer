package shapes3D;

import java.io.Serializable;
import java.util.Objects;

public abstract class Point3D extends Object implements Cloneable{
	
	protected Point3D() {
		
	};
	
	public abstract double getX();
	
	public abstract double getY();
	
	public abstract double getZ();
	
	public abstract void setLocation(double x, 
			double y, 
			double z);
	
	public void setLocation(Point3D p) {
		setLocation(p.getX(), p.getY(), p.getZ());
	};
	
	public static double distanceSq(double x1, 
			double y1, 
			double z1, 
			double x2, 
			double y2, 
			double z2) {
		
		return (x2-x1)*(x2-x1) 
			 + (y2-y1)*(y2-y1) 
			 + (z2-z1)*(z2-z1);
		
	}
	
	public static double distance(double x1, 
			double y1, 
			double z1, 
			double x2, 
			double y2, 
			double z2) {
		
		return Math.sqrt(distanceSq(x1, y1, z1, x2, y2, z2));
		
	}
	
	public double distanceSq(double px, 
			double py, 
			double pz) {
		
		return (px-getX())*(px-getX()) 
			 + (py-getY())*(py-getY()) 
			 + (pz-getZ())*(pz-getZ());
		
	}
	
	public double distanceSq(Point3D pt) {
		return (pt.getX()-getX())*(pt.getX()-getX()) 
			 + (pt.getY()-getY())*(pt.getY()-getY()) 
			 + (pt.getZ()-getZ())*(pt.getZ()-getZ());
	}
	

	public double distance(double px, 
			double py, 
			double pz) {
		
		return Math.sqrt(distanceSq(px, py, pz));
		
	}
	
	public double distance(Point3D pt) {
		return Math.sqrt(distanceSq(pt));
	}
	
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	public int hashCode() {
		return Objects.hash(getX(), getY(), getZ());
	}
	
	public boolean equals(Object obj) {
		if(!(obj instanceof Point3D)) return false;
		
		Point3D pt = (Point3D) obj;
		return (getX() == pt.getX() && getY() == pt.getY() && getZ() == pt.getZ());
	}
	
	public static class Double extends Point3D implements Serializable{
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private double x, y, z;
		
		public Double(){
			x = 0;
			y = 0;
			z = 0;
		}
		
		public Double(double x, double y, double z) {
			this.x = x;
			this.y = y;
			this.z = z;
		}

		@Override
		public double getX() {
			return x;
		}

		@Override
		public double getY() {
			return y;
		}

		@Override
		public double getZ() {
			return z;
		}

		@Override
		public void setLocation(double x, 
				double y, 
				double z) {
			this.x = x;
			this.y = y;
			this.z = z;
		}
		
		public String toString() {
			return "Point3D.Double[" + x + ", " + y + ", " + z + "]";
		}
		
	}

}
