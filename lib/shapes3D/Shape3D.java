package shapes3D;

public interface Shape3D {
	
	/**
	 * returns the BoundingCuboid of a shape
	 * @return Cuboid BoundingCuboid
	 */
	Cuboid getBounds3D();
	
	/**
	 * checks if a shape contains a set of 3D coordinates 
	 * @param x double 
	 * @param y double 
	 * @param z double 
	 * @return true if shape contains (x, y, z)
	 */
	boolean contains(double x, 
			double y, 
			double z);
	
	/**
	 * checks if a shape contains a 3D point
	 * @param p Point3D
	 * @return
	 */
	boolean contains(Point3D p);
	
	/**
	 * checks if a shape intersects a Cuboid defined by input parameters 
	 * (see definition of a Cuboid)
	 * @param x double
	 * @param y double 
	 * @param z double 
	 * @param l double 
	 * @param w double 
	 * @param h double 
	 * @return true if shape intersects Cuboid
	 */
	boolean intersects(double x, 
			double y, 
			double z, 
			double l, 
			double w, 
			double h);
	/**
	 * checks if a shape intersects a Cuboid 
	 * @param c Cuboid
	 * @return true if shape intersects Cuboid
	 */
	boolean intersects(Cuboid c);
	
	/**
	 * checks if a shape contains a Cuboid defined by input parameters completely
	 * (see definition of Cuboid)
	 * @param x double
	 * @param y double 
	 * @param z double 
	 * @param l double 
	 * @param w double 
	 * @param h double 
	 * @return true if shape contains Cuboid completely
	 */
	boolean contains(double x, 
			double y, 
			double z,
			double l, 
			double w, 
			double h);
	
	/**
	 * checks if a shape contains a Cuboid
	 * @param c Cuboid
	 * @return true if shape contains Cuboid completely
	 */
	boolean contains(Cuboid c);
	
	/**
	 * computes the center of mass of a shape
	 * @return Point3D center of mass 
	 */
	Point3D center();
	
}
