package shapes3D;

import java.io.Serializable;
import java.util.Objects;

public abstract class Line3D extends Object implements Shape3D, Cloneable{
		
	protected Line3D() {
		
	}
	
	public abstract double getX1();
	
	public abstract double getY1();
	
	public abstract double getZ1();
	
	public abstract Point3D getP1();
	
	public abstract double getX2();
	
	public abstract double getY2();
	
	public abstract double getZ2();
	
	public abstract Point3D getP2();
	
	public abstract void setLine(double x1, 
			double y1, 
			double z1, 
			double x2, 
			double y2, 
			double z2);
	
	public void setLine(Point3D p1, Point3D p2) {
		setLine(p1.getX(), p1.getY(), p1.getZ(), 
				p2.getX(), p2.getY(), p2.getZ());
	}
	
	public void setLine(Line3D l) {
		setLine(l.getP1(), l.getP2());
	}
	
	public static boolean linesIntersect(double x1,
			double y1, 
			double z1, 
			double x2, 
			double y2, 
			double z2, 
			double x3, 
			double y3, 
			double z3, 
			double x4, 
			double y4, 
			double z4) {
		return ((x2*y3*z4+y2*z3*x4+z2*x3*y4
				-z2*y3*x4-x2*z3*y4-y2*x3*z4)
				 + (x1*y3*z4+y1*z3*x4+z1*x3*y4
			-z1*y3*x4-y1*x3*z4-x1*z3*y4)
				 + (x1*y2*z4+y1*z2*x4+z1*x3*y4
			-z1*y2*x4-y1*x2*z4-x1*z3*y4)
				 + (x1*y2*z3+y1*z2*x3+z1*x2*y3
			-z1*y2*x3+y1*x2*z3+x1*z2*y3) == 0);
	}
	
	public boolean intersectsLine(double x1, 
			double y1, 
			double z1,
			double x2, 
			double y2, 
			double z2) {
		return linesIntersect(getX1(), 
				getY1(), 
				getZ1(), 
				getX2(),
				getY2(), 
				getZ2(), 
				x1, y1, z1, 
				x2, y2, z2);
	}
	
	public boolean intersectsLine(Line3D l) {
		return intersectsLine(l.getX1(),
				l.getY1(), 
				l.getZ1(), 
				l.getX2(), 
				l.getY2(), 
				l.getZ2());
	}
	
	public static double ptSegDistSq(double x1,
            double y1,
            double z1,
            double x2,
            double y2,
            double z2,
            double px,
            double py, 
            double pz) {
		double c1 = (x2-x1)*(px-x1)
				   +(y2-y1)*(py-y1)
				   +(z2-z1)*(pz-z1);
		if(c1 <= 0) 
			return Point3D.distance(x1, y1, z1, px, py, pz);
		
		double c2 = (x2-x1)*(x2-x1)
				   +(y2-y1)*(y2-y1)
				   +(z2-z1)*(z2-z1);
		if(c2 <= c1)
			return Point3D.distance(px, py, pz, x2, y2, z2);
		
		double b = c1/c2;

		return Point3D.distance(px, py, pz, 
				x1+b*(x2-x1), 
				y1+b*(y2-y1), 
				z1+b*(z2-z1));
	}
	
	public static double ptSegDist(double x1,
            double y1,
            double z1,
            double x2,
            double y2,
            double z2, 
            double px,
            double py, 
            double pz) {
		return Math.sqrt(ptSegDistSq(x1, y1, z1, x2, y2, z2, px, py, pz));
	}
	
	public double ptSegDistSq(double px,
            double py, 
            double pz) {
		return ptSegDistSq(getX1(), 
				getY1(), 
				getZ1(), 
				getX2(), 
				getY2(), 
				getZ2(), 
				px, py, pz);
	}
	
	public double ptSegDistSq(Point3D pt) {
		return ptSegDistSq(getX1(), 
				getY1(), 
				getZ1(), 
				getX2(), 
				getY2(), 
				getZ2(), 
				pt.getX(), 
				pt.getY(), 
				pt.getZ());
	}
	
	public double ptSegDist(double px, 
			double py, 
			double pz) {
		return ptSegDist(getX1(), 
				getY1(), 
				getZ1(), 
				getX2(), 
				getY2(), 
				getZ2(), 
				px, py, pz);
	}
	
	public double ptSegDist(Point3D pt) {
		return ptSegDist(getX1(), 
				getY1(), 
				getZ1(), 
				getX2(), 
				getY2(), 
				getZ2(), 
				pt.getX(), 
				pt.getY(), 
				pt.getZ());
	}
	
	public static double ptLineDistSq(double x1, 
			double y1, 
			double z1, 
			double x2, 
			double y2, 
			double z2, 
			double px, 
			double py, 
			double pz) {
		return (((y2-y1)*(z1-pz)-(z2-z1)*(y1-py))
			   *((y2-y1)*(z1-pz)-(z2-z1)*(y1-py))
			   +((z2-z1)*(x1-px)-(x2-x1)*(z1-pz))
			   *((z2-z1)*(x1-px)-(x2-x1)*(z1-pz))
			   +((x2-x1)*(y1-py)-(y2-y1)*(x1-px))
			   *((x2-x1)*(y1-py)-(y2-y1)*(x1-px)))
				/Point3D.distanceSq(x1, y1, z1, x2, y2, z2);
	}
	
	public static double ptLineDist(double x1, 
			double y1, 
			double z1, 
			double x2, 
			double y2, 
			double z2, 
			double px, 
			double py, 
			double pz) {
		return Math.sqrt(ptLineDistSq(x1, y1, z1, x2, y2, z2, px, py, pz));
	}
	
	public double ptLineDistSq(double px, 
			double py, 
			double pz) {
		return ptLineDistSq(getX1(), 
				getY1(), 
				getZ1(), 
				getX2(), 
				getY2(), 
				getZ2(), 
				px, py, pz);
	}
	
	public double ptLineDistSq(Point3D pt) {
		return ptLineDistSq(getX1(), 
				getY1(), 
				getZ1(), 
				getX2(), 
				getY2(), 
				getZ2(), 
				pt.getX(), 
				pt.getY(), 
				pt.getZ());
	}
	
	public double ptLineDist(double px, 
			double py, 
			double pz) {
		return ptLineDist(getX1(), 
				getY1(), 
				getZ1(), 
				getX2(), 
				getY2(), 
				getZ2(), 
				px, py, pz);
	}
	
	public double ptLineDist(Point3D pt) {
		return ptLineDist(getX1(), 
				getY1(), 
				getZ1(), 
				getX2(), 
				getY2(), 
				getZ2(), 
				pt.getX(), 
				pt.getY(), 
				pt.getZ());
	}
	
	public boolean equals(Object obj) {
		if(!(obj instanceof Line3D)) return false;
		
		Line3D l = (Line3D) obj;
		return (getP1().equals(l.getP1()) && getP2().equals(l.getP2())) 
		    || (getP1().equals(l.getP2()) && getP2().equals(l.getP1()));
	}
	
	public int hashCode() {
		double minX, minY, minZ, maxX, maxY, maxZ;

		if(getX1() > getX2()) {
			maxX = getX1();
			minX = getX2();			
		}else {
			maxX = getX2();
			minX = getX1();
		}
		if(getY1() > getY2()) {
			maxY = getY1();
			minY = getY2();			
		}else {
			maxY = getY2();
			minY = getY1();
		}
		if(getZ1() > getZ2()) {
			maxZ = getZ1();
			minZ = getZ2();			
		}else {
			maxZ = getZ2();
			minZ = getZ1();
		}
		return Objects.hash(minX, minY, minZ, maxX, maxY, maxZ);
	}
	
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	public static class Double extends Line3D implements Serializable{
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		double x1, x2, y1, y2, z1, z2;
		
		public Double() {
			x1 = 0;
			x2 = 0;
			y1 = 0;
			y2 = 0;
			z1 = 0;
			z2 = 0;
		}
		
		public Double(double x1, 
				double y1, 
				double z1, 
				double x2, 
				double y2, 
				double z2) {
			
			this.x1 = x1;
			this.y1 = y1;
			this.z1 = z1;
			this.x2 = x2;
			this.y2 = y2;
			this.z2 = z2;
			
		}
		
		public Double(Point3D p1, Point3D p2) {
			
			this.x1 = p1.getX();
			this.y1 = p1.getY();
			this.z1 = p1.getZ();
			this.x2 = p2.getX();
			this.y2 = p2.getY();
			this.z2 = p2.getZ();
			
		}
		
		@Override
		public double getX1() {
			return x1;
		}

		@Override
		public double getY1() {
			return y1;
		}

		@Override
		public double getZ1() {
			return z1;
		}

		@Override
		public Point3D getP1() {
			return new Point3D.Double(x1, y1, z1);
		}

		@Override
		public double getX2() {
			return x2;
		}

		@Override
		public double getY2() {
			return y2;
		}

		@Override
		public double getZ2() {
			return z2;
		}

		@Override
		public Point3D getP2() {
			return new Point3D.Double(x2, y2, z2);
		}

		@Override
		public void setLine(double x1, double y1, double z1, double x2, double y2, double z2) {
			this.x1 = x1;
			this.y1 = y1;
			this.z1 = z1;
			this.x2 = x2;
			this.y2 = y2;
			this.z2 = z2;
		}

		@Override
		public Cuboid getBounds3D() {
			double minX, minY, minZ, maxX, maxY, maxZ;

			if(getX1() > getX2()) {
				maxX = getX1();
				minX = getX2();			
			}else {
				maxX = getX2();
				minX = getX1();
			}
			if(getY1() > getY2()) {
				maxY = getY1();
				minY = getY2();			
			}else {
				maxY = getY2();
				minY = getY1();
			}
			if(getZ1() > getZ2()) {
				maxZ = getZ1();
				minZ = getZ2();			
			}else {
				maxZ = getZ2();
				minZ = getZ1();
			}
			return new Cuboid.Double(minX, minY, minZ, maxX-minX, maxY-minY, maxZ-minZ);
		}

		@Override
		public boolean contains(double x, double y, double z) {
			return false;
		}

		@Override
		public boolean contains(Point3D p) {
			return false;
		}

		@Override
		public boolean intersects(double x, double y, double z, double l, double w, double h) {
			return intersects(new Cuboid.Double(x, y, z, l, w, h));
		}

		@Override
		public boolean intersects(Cuboid r) {
			return r.intersectsLine(this);
		}

		@Override
		public boolean contains(double x, double y, double z, double l, double w, double h) {
			return false;
		}

		@Override
		public boolean contains(Cuboid r) {
			return false;
		}
		
		public String toString() {
			return "Line3D.Double[x1=" + x1 + ", y1=" + y1 + ", z1=" + z1 
					+ ", x2=" + x2 + ", y2=" + y2 + ", z2=" + z2 + "]";
		}

		@Override
		public Point3D center() {
			return new Point3D.Double(getX1() + (getX2()-getX1())/2, getY1() + (getY2()-getY1())/2, getZ1() + (getZ2()-getZ1())/2);
		}
	}

}
