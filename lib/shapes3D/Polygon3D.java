package shapes3D;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Polygon3D extends Object implements Shape3D, Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected Cuboid bounds;
	private int npoints;
	private List<Point3D> points;
	
	public Polygon3D() {
		bounds = new Cuboid.Double();
		npoints = 0;
		points = new ArrayList<Point3D>();
	}
	
	public Polygon3D(List<Point3D> ps) {
		points = ps;
		npoints = ps.size();
		bounds = getBounds3D();
	}
	
	public void reset() {
		points.clear();
		npoints = 0;
	}
	
	public void translate(double deltaX, double deltaY, double deltaZ) {
		for(Point3D p:points) {
			p.setLocation(p.getX()+deltaX, p.getY()+deltaY, p.getZ()+deltaZ);
		}
	}
	
	public void addPoint(Point3D p) {
		npoints++;
		points.add(p);
		bounds.extendToInclude(p);
	}

	@Override
	public Cuboid getBounds3D() {
		double minX = Double.MAX_VALUE; 
		double minY = Double.MAX_VALUE; 
		double minZ = Double.MAX_VALUE;
		double maxX = Double.MIN_VALUE; 
		double maxY = Double.MIN_VALUE; 
		double maxZ = Double.MIN_VALUE;
				
		for(Point3D p:points) {
			if(p.getX() < minX) minX = p.getX();
			if(p.getY() < minY) minY = p.getY();
			if(p.getZ() < minZ) minZ = p.getZ();
			if(p.getX() > maxX) maxX = p.getX();
			if(p.getY() > maxY) maxY = p.getY();
			if(p.getZ() > maxZ) maxZ = p.getZ();
		}
		
		return new Cuboid.Double(minX, minY, minZ, maxX-minX, maxY-minY, maxZ-minZ);
	}

	public List<Point3D> getPoints(){
		return points;
	}
	
	@Override
	public boolean contains(double x, double y, double z) {
		return false;
	}

	@Override
	public boolean contains(Point3D p) {
		return false;
	}

	@Override
	public boolean intersects(double x, double y, double z, double l, double w, double h) {
		Cuboid c = new Cuboid.Double(x, y, z, l, w, h);
		for(int i = 1; i < npoints; i++) {
			if(c.intersectsLine(new Line3D.Double(points.get(i-1), points.get(i))))return true;
		}
		return false;
	}

	@Override
	public boolean intersects(Cuboid r) {
		return intersects(r.getX(), r.getY(), r.getZ(), r.getLength(), r.getWidth(), r.getHeight());
	}

	@Override
	public boolean contains(double x, double y, double z, double l, double w, double h) {
		return false;
	}

	@Override
	public boolean contains(Cuboid r) {
		return false;
	}

	@Override
	public Point3D center() {
		double x = 0;
		double y = 0;
		double z = 0;
		for(Point3D p:points) {
			x += p.getX();
			y += p.getY();
			z += p.getZ();
		}
		return new Point3D.Double(x/npoints, y/npoints, z/npoints);
	}

}
