package shapes3D;

public class Sphere implements Shape3D{

	private Point3D center;
	private double radius;
	
	
	public Sphere() {
		
	}
	
	public Sphere(Point3D c, double r) {
		center = c;
		radius = r;
	}
	
	public Sphere(double x, double y, double z, double r) {
		center = new Point3D.Double(x, y, z);
		radius = r;
	}
	
	@Override
	public Cuboid getBounds3D() {
		return new Cuboid.Double(center.getX()-radius, 
							     center.getY()-radius, 
							     center.getZ()-radius, 
							     2*radius, 
							     2*radius, 
							     2*radius);
	}
	
	public Point3D getCenter() {
		return center;
	}
	
	public double getRadius() {
		return radius;
	}
	
	public void setCenter(Point3D p) {
		this.center = p;
	}
	
	public void setRadius(double r) {
		this.radius = r;
	}

	@Override
	public boolean contains(double x, double y, double z) {
		return center.distance(x, y, z) <= radius;
	}

	@Override
	public boolean contains(Point3D p) {
		return contains(p.getX(), p.getY(), p.getZ());
	}
	
	public double[] intersects(Point3D origin, double nx, double ny, double nz) {
		double normL = Math.sqrt(nx*nx+ny*ny+nz*nz);
		double rho = (nx*center.getX()+ny*center.getY()+nz*center.getZ())
						/normL;
		if(rho > radius || rho < -radius)return null;
		
		double r = Math.sqrt(radius*radius-rho*rho);
		
		double cx = center.getX()+rho*(nx/normL);
		double cy = center.getY()+rho*(ny/normL);
		double cz = center.getZ()+rho*(nz/normL);
		
		return new double[] {cx, cy, cz, r};
	}
	
	public double[] intersects(Plane3D plane) {
		return intersects(plane.origin(), plane.normalX(), plane.normalY(), plane.normalZ());
	}

	@Override
	public boolean intersects(double x, double y, double z, double l, double w, double h) {
		
		double deltaX = center.getX() - Math.max(x, Math.min(center.getX(), x+l));
		double deltaY = center.getY() - Math.max(y, Math.min(center.getY(), y+w));
		double deltaZ = center.getZ() - Math.max(z, Math.min(center.getZ(), z+h));
		
		return (deltaX*deltaX+deltaY*deltaY+deltaZ*deltaZ) < (radius*radius);
	}

	@Override
	public boolean intersects(Cuboid r) {
		return intersects(r.getX(), r.getY(), r.getZ(), r.getLength(), r.getWidth(), r.getHeight());
	}

	@Override
	public boolean contains(double x, 
			double y, 
			double z, 
			double l, 
			double w, 
			double h) {
		return contains(x, y, z) 
				&& contains(x+l, y, z)
				&& contains(x, y+w, z)
				&& contains(x, y, z+h)
				&& contains(x+l, y+w, z)
				&& contains(x, y+w, z+h)
				&& contains(x+l, y, z+h)
				&& contains(x+l, y+w, z+h);
	}

	@Override
	public boolean contains(Cuboid r) {
		return contains(r.getX(),
				r.getY(), 
				r.getZ(), 
				r.getLength(), 
				r.getWidth(), 
				r.getHeight());
	}

	@Override
	public Point3D center() {
		return center;
	}

	public String toString() {
		return "Sphere[Center: " + center + ", Radius: " + radius + "]";
	}
	
}
