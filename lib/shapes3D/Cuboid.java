package shapes3D;

import java.io.Serializable;

public abstract class Cuboid implements Shape3D{
	
	protected Cuboid() {
		
	}
	
	public abstract double getX();
	
	public abstract double getY();
	
	public abstract double getZ();
	
	public abstract double getLength();
	
	public abstract double getWidth();
	
	public abstract double getHeight();
	
	public abstract double getCenterX();
	
	public abstract double getCenterY();
	
	public abstract double getCenterZ();

	public abstract void setCuboid(double x, 
			double y, 
			double z, 
			double l,
			double w, 
			double h);
	
	public void setCuboid(Cuboid c) {
		setCuboid(c.getX(), c.getY(), c.getZ(), 
				  c.getLength(), c.getWidth(), c.getHeight());
	}
	
	public abstract boolean extendToInclude(double x, 
			double y, 
			double z);
	
	public boolean extendToInclude(Point3D p) {
		return extendToInclude(p.getX(), p.getY(), p.getZ());
	}
	
	public abstract boolean extendToInclude(double x, 
			double y, 
			double z, 
			double l, 
			double w, 
			double h);
	
	public boolean extendToInclude(Cuboid c) {
		return extendToInclude(c.getX(), 
				c.getY(), 
				c.getZ(), 
				c.getLength(),
				c.getWidth(),
				c.getHeight());
	}
		
	public boolean contains(double x, 
			double y, 
			double z) {
		
		return (getX() <= x && x <= getX()+getLength()
			&& getY() <= y && y <= getY()+getWidth()
			&& getZ() <= z && z <= getZ()+getHeight());
	}
	
	public boolean contains(Point3D p) {
		return contains(p.getX(), p.getY(), p.getZ());
	}
	
	public boolean intersectsLine(double x1, 
			double y1, 
			double z1, 
			double x2, 
			double y2, 
			double z2) {
		if(contains(x1, y1, z1) || contains(x2, y2, z2)) return true;
		
		//intersects Floor?
		if(contains(new Plane3D.Double(getX(), getY(), getZ(), 0, 0, -1).lineIntersection(x1, y1, z1, x2, y2, z2)))return true;
		//intersects Roof?
		if(contains(new Plane3D.Double(getX()+getLength(), getY()+getWidth(), getZ()+getHeight(), 0, 0, 1).lineIntersection(x1, y1, z1, x2, y2, z2)))return true;
		//intersects Front?
		if(contains(new Plane3D.Double(getX(), getY(), getZ(), 0, -1, 0).lineIntersection(x1, y1, z1, x2, y2, z2)))return true;
		//intersects Back?
		if(contains(new Plane3D.Double(getX()+getLength(), getY()+getWidth(), getZ()+getHeight(), 0, 1, 0).lineIntersection(x1, y1, z1, x2, y2, z2)))return true;
		//intersects Left Side?
		if(contains(new Plane3D.Double(getX(), getY(), getZ(), -1, 0, 0).lineIntersection(x1, y1, z1, x2, y2, z2)))return true;
		//intersects Right Side?
		if(contains(new Plane3D.Double(getX()+getLength(), getY()+getWidth(), getZ()+getHeight(), 1, 0, 0).lineIntersection(x1, y1, z1, x2, y2, z2)))return true;
		return false;
	}
	
	public boolean intersectsLine(Line3D l) {
		return intersectsLine(l.getX1(), l.getY1(), l.getZ1(), l.getX2(), l.getY2(), l.getZ2());
	}
	
	public boolean containsLine(double x1, 
			double y1, 
			double z1, 
			double x2, 
			double y2,
			double z2) {
		return contains(x1, y1, z1) && contains(x2, y2, z2);
	}
	
	public boolean containsLine(Line3D l) {
		return containsLine(l.getX1(), l.getY1(), l.getZ1(), 
				l.getX2(), l.getY2(), l.getZ2());
	} 
	
	public boolean intersects(double x, 
			double y, 
			double z, 
			double l, 
			double w, 
			double h) {
		return contains(x, y, z) 
			|| contains(x+l, y, z)
			|| contains(x, y+w, z)
			|| contains(x, y, z+h)
			|| contains(x+l, y+w, z)
			|| contains(x, y+w, z+h)
			|| contains(x+l, y, z+h)
			|| contains(x+l, y+w, z+h);
	}
	
	public boolean intersects(Cuboid c) {
		return intersects(c.getX(), c.getY(), c.getZ(), 
				c.getLength(), c.getWidth(), c.getHeight());
	}
	
	public boolean contains(double x, 
			double y, 
			double z, 
			double l, 
			double w, 
			double h) {
		return contains(x, y, z) 
			&& contains(x+l, y, z)
			&& contains(x, y+w, z)
			&& contains(x, y, z+h)
			&& contains(x+l, y+w, z)
			&& contains(x, y+w, z+h)
			&& contains(x+l, y, z+h)
			&& contains(x+l, y+w, z+h);
	}
	
	public boolean contains(Cuboid c) {
		return contains(c.getX(), c.getY(), c.getZ(), 
				c.getLength(), c.getWidth(), c.getHeight());
	}
	
	public static class Double extends Cuboid implements Serializable{
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private double x, y, z, l, w, h;
		
		/**
		 * creates
		 */
		public Double() {
			x = 0;
			y = 0;
			z = 0;
			l = 0;
			w = 0;
			h = 0;
		}
		
		public Double(double x, 
				double y, 
				double z, 
				double l, 
				double w, 
				double h) {
			this.x = x;		
			this.y = y;
			this.z = z;
			this.l = l;
			this.w = w;
			this.h = h;
		}

		@Override
		public Cuboid getBounds3D() {
			return new Cuboid.Double(x, y, z, l, w, h);
		}

		@Override
		public double getX() {
			return x;
		}

		@Override
		public double getY() {
			return y;
		}

		@Override
		public double getZ() {
			return z;
		}

		@Override
		public double getLength() {
			return l;
		}

		@Override
		public double getWidth() {
			return w;
		}

		@Override
		public double getHeight() {
			return h;
		}

		@Override
		public double getCenterX() {
			return x + l/2;
		}

		@Override
		public double getCenterY() {
			return y + w/2;
		}

		@Override
		public double getCenterZ() {
			return z + h/2;
		}

		@Override
		public void setCuboid(double x, 
				double y, 
				double z, 
				double l, 
				double w, 
				double h) {
			this.x = x;		
			this.y = y;
			this.z = z;
			this.l = l;
			this.w = w;
			this.h = h;
		}
		
		@Override
		public boolean extendToInclude(double x, 
				double y, 
				double z) {
			if(this.contains(x, y, z))return false;
			double deltaX = x - this.x;
			double deltaY = y - this.y;
			double deltaZ = z - this.z;
			if(deltaX < 0) {
				this.x = x;
				this.l = l-deltaX;
			}else {
				if(deltaX > l)this.l = deltaX;
			}
			if(deltaY < 0) {
				this.y = y;
				this.w = w-deltaY;
			}else {
				if(deltaY > w)this.w = deltaY;
			}
			if(deltaZ < 0) {
				this.z = z;
				this.h = h-deltaZ;
			}else {
				if(deltaZ > h)this.h = deltaZ;
			}
			return true;
		}
					
		public String toString() {
			return "Cuboid.Double[x=" + x + ", y=" + y + ", z=" + z 
					+ ", length=" + l + ", width=" + w + ", height=" + h + "]"; 
		}

		@Override
		public Point3D center() {
			return new Point3D.Double(getCenterX(), getCenterY(), getCenterZ());
		}

		@Override
		public boolean extendToInclude(double x, double y, double z, double l, double w, double h) {
			if(this.contains(x, y, z, l, w, h))return false;
			extendToInclude(x, y, z);
			extendToInclude(x+l, y+w, z+h);
			return true;
		}
		
	}
	
}
