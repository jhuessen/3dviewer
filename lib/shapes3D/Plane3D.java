package shapes3D;

import java.io.Serializable;

public abstract class Plane3D extends Object implements Shape3D, Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected Plane3D() {
		
	}
	
	public abstract Point3D origin();
		
	public abstract Vector normal();
	
	public abstract double normalX();
	
	public abstract double normalY();
	
	public abstract double normalZ();
	
	public abstract double constant();
	
	public abstract Point3D lineIntersection(double x1, 
			double y1, 
			double z1, 
			double x2, 
			double y2, 
			double z2);
	
	public Point3D lineIntersection(Line3D l) {
		return lineIntersection(l.getX1(), 
				l.getY1(), 
				l.getZ1(), 
				l.getX2(), 
				l.getY2(), 
				l.getZ2());
	}
	
	/**
	 * checks on which side of the Plane a Point p lies with respect to the direct of its normal vector
	 * @param Point 
	 * @return 1 if p in same direction as normal, -1 if in opposite direction, 0 if p is part of the plane
	 */
	public abstract int side(double x, double y, double z);
	
	public int side(Point3D p) {
		return side(p.getX(), p.getY(), p.getZ());
	};
		
	public static class Double extends Plane3D{

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		private Point3D origin;
		private Vector normal;
		private double d;
		
		public Double() {
			origin = new Point3D.Double();
			normal = new Vector(0, 0, 1);
			d = 0;
		}
		
		public Double(Point3D o, Vector normal) {
			this.origin = o;
			this.normal = normal.normalize();
			this.d = computeConstant();
		}
		
		public Double(double x0, double y0, double z0, Vector normal) {
			this.origin = new Point3D.Double(x0, y0, z0);
			this.normal = normal.normalize();
			this.d = computeConstant();
		}
		
		public Double(double x0, double y0, double z0, double vx, double vy, double vz) {
			this.origin = new Point3D.Double(x0, y0, z0);
			this.normal = new Vector(vx, vy, vz).normalize();
			this.d = computeConstant();
		}
		
		public Double(Point3D p1, Point3D p2, Point3D p3) {
			this.origin = p1;
			Vector v1 = new Vector(p2.getX()-p1.getX(), p2.getY()-p1.getY(), p2.getZ()-p1.getZ());
			Vector v2 = new Vector(p3.getX()-p1.getX(), p3.getY()-p1.getY(), p3.getZ()-p1.getZ());
			this.normal = (v1.cross(v2)).normalize();
			this.d = computeConstant();
		}
		
		@Override
		public Point3D origin() {
			return origin;
		}

		@Override
		public Vector normal() {
			return normal;
		}
		
		@Override
		public double normalX() {
			return normal.getValue(0);
		}
		
		@Override
		public double normalY() {
			return normal.getValue(1);
		}
		
		@Override
		public double normalZ() {
			return normal.getValue(2);
		}
		
		@Override
		public double constant() {
			return d;
		}
		
		
		public void setOrigin(Point3D o) {
			this.origin = o;
			this.d = computeConstant();
		}
		
		public void setNormal(Vector v) {
			this.normal = v.normalize();
			this.d = computeConstant();
		}
		
		@Override
		public Cuboid getBounds3D() {
			return null;
		}

		private double computeConstant() {
			return -(normal.getValue(0)*origin.getX() + normal.getValue(1)*origin.getY() + normal.getValue(2)*origin.getZ());
		}
		
		@Override
		public boolean contains(double x, double y, double z) {
			return ((x-origin.getX())*normal.getValue(0) + (y-origin.getY())*normal.getValue(1) + (z-origin.getZ())*normal.getValue(2)) == 0;
		}

		@Override
		public boolean contains(Point3D p) {
			return contains(p.getX(), p.getY(), p.getZ());
		}

		public int side(double x, double y, double z) {
			if(contains(x, y, z))return 0;
			double testV = normal.getValue(0)*(x-origin.getX()) 
					 + normal.getValue(1)*(y-origin.getY())
				     + normal.getValue(2)*(z-origin.getZ());
			return (testV > 0) ? 1:-1;
		}
		
		@Override
		public boolean intersects(double x, double y, double z, double l, double w, double h) {
			int defaultSide = side(x, y, z);
			if(side(x+l, y+w, z+h) != defaultSide)return true;
			if(side(x+l, y, z) != defaultSide)return true;
			if(side(x, y+w, z) != defaultSide)return true;
			if(side(x, y, z+h) != defaultSide)return true;
			if(side(x+l, y+w, z) != defaultSide)return true;
			if(side(x+l, y, z+h) != defaultSide)return true;
			if(side(x, y+w, z+h) != defaultSide)return true;
			return false;
		}

		@Override
		public boolean intersects(Cuboid r) {
			return intersects(r.getX(), r.getY(), r.getZ(), r.getLength(), r.getWidth(), r.getHeight());
		}

		@Override
		public boolean contains(double x, double y, double z, double l, double w, double h) {
			return false;
		}

		@Override
		public boolean contains(Cuboid r) {
			return false;
		}

		@Override
		public Point3D center() {
			return origin;
		}

		@Override
		public Point3D lineIntersection(double x1, double y1, double z1, double x2, double y2, double z2) {
			Vector l0p0 = new Vector(origin.getX()-x1, origin.getY()-y1, origin.getZ()-z1);
			Vector l = new Vector(x2-x1, y2-y1, z2-z1).normalize();
			double d = l0p0.dot(normal)/l.dot(normal);
			return new Point3D.Double(x1+d*l.getValue(0), y1+d*l.getValue(1), z1+d*l.getValue(2));
		}
		
		@Override
		public Point3D lineIntersection(Line3D l) {
			return lineIntersection(l.getX1(), l.getY1(), l.getZ1(), l.getX2(), l.getY2(), l.getZ2());
		}

		public String toString() {
			return "Plane3D.Double[Origin: " + origin + ", Normal: " + normal + "]";
		}

		
		
	}
	
}
