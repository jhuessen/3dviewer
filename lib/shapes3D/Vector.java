package shapes3D;

import java.util.Arrays;

public class Vector {

	private double[] values;
	private int dim;
	
	public Vector() {
		dim = 0;
		values = new double[0];
	}
	
	public Vector(double... vs) {
		values = vs;
		dim = vs.length;
	}
	
	public double[] getValues() {
		return values;
	}
	
	public double getValue(int i) {
		return values[i];
	}
	
	public int dimension() {
		return dim;
	}
	
	public double length() {
		double res = 0;
		for(double d:values) {
			res += d*d;
		}
		return Math.sqrt(res);
	}
	
	public Vector normalize() {
		double l = length();
		for(int i = 0; i < dim; i++) {
			values[i] = values[i]/l;
		}
		return this;
	}
		
	public void add(double v) {
		double[] newValues = new double[++dim];
		for(int i = 0; i < values.length; i++) {
			newValues[i] = values[i];
		}
		newValues[dim-1] = v;
		values = newValues;
	}
	
	public boolean remove(int i) {
		if(i > dim-1) {
			System.out.println("Index exceeds Dimension");
			return false;
		}
		double[] newValues = new double[--dim];
		int k = 0;
		for(int j = 0; j < dim+1; j++) {
			if(j != i) {
				newValues[k] = values[j];
				k++;
			} 			
		}
		values = newValues;
		return true;
	}
	
	public Vector add(Vector v) {
		if(dim != v.dimension()) {
			throw new RuntimeException("Vectors must have the same dimension");
		}
		double[] res = new double[dim];
		for(int i = 0; i < dim; i++) {
			res[i] = values[i] + v.getValue(i);
		}
		return new Vector(res);
	}
	
	public Vector subtract(Vector v) {
		if(dim != v.dimension()) {
			throw new RuntimeException("Vectors must have the same dimension");
		}
		double[] res = new double[dim];
		for(int i = 0; i < dim; i++) {
			res[i] = values[i] - v.getValue(i);
		}
		return new Vector(res);
	}
	
	public double dot(Vector v) {
		if(dim != v.dimension()) {
			throw new RuntimeException("Vectors must have the same dimension");
		}
		double res = 0;
		for(int i = 0; i < values.length; i++) {
			res += values[i]*v.getValues()[i];
		}
		return res;
	}

	public Vector cross(Vector v) {
		if(dim != 3 || v.dimension() != 3) {
			throw new RuntimeException("Cross product is only defined in three-dimensional space");
		}
		return new Vector(values[1]*v.getValues()[2]-values[2]*v.getValues()[1], 
				values[2]*v.getValues()[0]-values[0]*v.getValues()[2], 
				values[0]*v.getValues()[1]-values[1]*v.getValues()[0]);
	}

	@Override
	public String toString() {
		return "Vector [values=" + Arrays.toString(values) + ", dimension=" + dim + "]";
	}
		
}
